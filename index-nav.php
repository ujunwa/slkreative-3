</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="http://assets/images/loo.jpg"><img src="assets/images/loo.jpg" alt="SL Kreativez Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="http://assets/images/loo.jpg"><img src="assets/images/loo.jpg" alt="SL Kreativez Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="visible-lg visible-md hidden-xs hidden-sm ">

						<ul>
							<li>
								<a href="index.php"><div><img src="images/ihome.png" style="margin-top:-15px;"></div></a>
							</li>
							<li><a href="about.php"><div><img src="images/iwho2.png" style="margin-top:-15px;"></div></a>
							</li>
							<li><a href="services.php"><div><img src="images/iwhat2.png" style="margin-top:-15px;"></div></a>
							</li>
							<li><a href="/sl"><div><img src="images/iblog.png" style="margin-top:-15px;"></div></a>
							</li>
							<li><a href="jobs.php"><div><img src="images/career.png" style="margin-top:-15px;"></div></a>
							</li>
							<li><a href="contact.php"><div><img src="images/icontact.png" style="margin-top:-15px;"></div></a>
							</li>
							<li><a href="projects.php"><div><img src="images/iproject.png" style="margin-top:-15px;"></div></a>
							</li>
							<li><a href="#"><div><img src="images/copy.png" style="margin-top:-15px;"></div></a>
							</li>
						</ul>


					</nav><!-- #primary-menu end -->

					<nav id="primary-menu" class="hidden-lg hidden-md visible-sm-visible-xs">

						<ul>
							<li><a href="index.php"><div>Home</div></a>
							</li>
							<li><a href="about.php"><div>Who We Are</div></a>
							</li>
							<li><a href="services.php"><div>What We Do</div></a>
							</li>
							<li><a href="/sl"><div>Blog</div></a>
							</li>
							<li><a href="jobs.php"><div>Careers</div></a>
							</li>
							<li><a href="contact.php"><div>Connect</div></a>
							</li>
							<li><a href="projects.php"><div>Projects</div></a>
							</li>
							<li><a href="#"><div>Credit</div></a>
							</li>
						</ul>

					</nav><!-- #primary-menu end -->
				</div>

			</div>

		</header><!-- #header end -->

