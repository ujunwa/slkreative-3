<?php include('header.php'); ?>

<!-- Document Title
	============================================= -->
	<title>Job Openings | SL Kreativez</title>


<?php include('nav.php'); ?>



		<!-- Page Title
		============================================= -->
		<section id="page-title" style="background-image: url('images/job1.jpg'); padding: 80px 0;" >

			<div class="container clearfix">
				<h1 style="color: white; font-weight: bolder;">Career Openings</h1>
				<span  style="color: white; font-weight: bolder;">Write/Work with us</span>
				<ol class="breadcrumb">
					<li><a href="#" style="color: white; font-weight: bolder;">Home</a></li>
					<li><a href="#"  style="color: white; font-weight: bolder;">Pages</a></li>
					<li class="active" style="color: white; font-weight: bolder;">Career</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
				<p>We're value driven and creativity centers at SL Kreativez. We assure you that working with us will never rupture your aspirations. 

				This is because we are flexible and have mutual respect for and towards your person and dreams. This could be the breaking ground to launch your journey.</p>


					<div class="col_three_fifth nobottommargin">

						<div class="fancy-title title-bottom-border">
							<h3>Creative Writers</h3>
						</div>

						<p>We're looking for art enthusiasts and professionals with different background who want to make a difference in the creative industry. Do you have the skill and flair? Or you have the flair but little skills? We can work together. </p>

						<div class="accordion accordion-bg clearfix">

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           Requirements</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Anyone with a flair for research and writing. </li>
									<li><img src="images/tick.png" />     A qualification will be a bonus to credibility.</li>
									<li><img src="images/tick.png" />     Attention to details.</li>
									<li><img src="images/tick.png" />     Creative and versatile.</li>
									<li><img src="images/tick.png" />     Understanding of what we're about.</li>
									<li><img src="images/tick.png" />     Good Communication Skills</li>
									<li><img src="images/tick.png" />     Team Player</li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What we Expect from you?</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     </li>
									<li><img src="images/tick.png" />     </li>
									<li><img src="images/tick.png" />     </li>
									
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What you've got?</div>
							<div class="acc_content clearfix">You'll be familiar with creative writing as well as a chance to work with great team members .</div>

						</div>

						<a href="#" data-scrollto="#job-apply" class="button button-3d nomargin">Apply Now</a>

						<div class="divider divider-short"><img src="images/star.png" /></div>


						<div class="fancy-title title-bottom-border">
							<h3>Social Media / Community Manager</h3>
						</div>

						<p>We want our content to get to our community in time. We want everyone to be a part of this journey and can only do so when everyone is up to date on where we're headed. Social Media makes that easy and fun.</p>

						<div class="accordion accordion-bg clearfix">

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           Requirements</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Prior Experience as a social media manager </li>
									<li><img src="images/tick.png" />     A qualification will be a bonus to credibility.</li>
									<li><img src="images/tick.png" />     Attention to details.</li>
									<li><img src="images/tick.png" />     Creative and versatile.</li>
									<li><img src="images/tick.png" />     Understanding of what we're about.</li>
									<li><img src="images/tick.png" />     Good Communication Skills</li>
									<li><img src="images/tick.png" />     Team Player</li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What we Expect from you?</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Manage all social media and content for day to day activities.</li>
									<li><img src="images/tick.png" />     Keep the community engaged with creative contents, opportunities, promos, competitions, events and blog posts</li>
									<li><img src="images/tick.png" />     Create and manage campaigns on the platforms.</li>
									<li><img src="images/tick.png" />     Manage all email list and newsletter campaigns.</li>
									
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What you've got?</div>
							<div class="acc_content clearfix">You'll be familiar with creative writing as well as a chance to work with great team members .</div>

						</div>

						<a href="#" data-scrollto="#job-apply" class="button button-3d nomargin">Apply Now</a>

						<div class="divider divider-short"><img src="images/star.png" /></div>


						<div class="fancy-title title-bottom-border">
							<h3>Web Content Editor</h3>
						</div>

						<p>We run a creative firm and it's our value to stick with good professional ethics. A WCE manages every details and information that visitors get on their visits. We want a locus that resonates with our values and an editor serves as our watchman/woman. </p>

						<div class="accordion accordion-bg clearfix">

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           Requirements</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     A degree in mass communications.</li>
									<li><img src="images/tick.png" />     Attention to accuracy of information and details.</li>
									<li><img src="images/tick.png" />     Creative and aesthetic judgment skills.</li>
									<li><img src="images/tick.png" />     Patience is a virtue.</li>
									<li><img src="images/tick.png" />     Understanding of what we're about.</li>
									<li><img src="images/tick.png" />     Good Communication Skills</li>
									<li><img src="images/tick.png" />     Team Player</li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What we Expect from you?</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Edit contents before they are posted on the website most especially third-party contents and information.</li>
									<li><img src="images/tick.png" />     Sanction any contrary values.</li>
									<li><img src="images/tick.png" />     Provide assistance to all internal users of the website on graphics and optimization criteria.</li>
									<li><img src="images/tick.png" />     Work with management on enhancing customers experience.</li>
									
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What you've got?</div>
							<div class="acc_content clearfix">You'll be familiar with creative writing as well as a chance to work with great team members .</div>

						</div>

						<a href="#" data-scrollto="#job-apply" class="button button-3d nomargin">Apply Now</a>

						<div class="divider divider-short"><img src="images/star.png" /></div>



						<div class="fancy-title title-bottom-border">
							<h3>Creative Tutors</h3>
						</div>

						<p>Creative Tutors are needed for our project on creative writing training for youngsters. This project will commence in 2018 and run through the year. 

						Update on subsequent years will come before the end of the year.
						Tutors field includes:<br>
						A Rector<br>
						Poetry Tutor<br>
						Drama Tutor<br>
						Prose Tutor<br>
						Language Tutor</p>

						<div class="accordion accordion-bg clearfix">

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           Requirements</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Graduates of creative and theatre arts only with interest at passing learning to others.</li>
									<li><img src="images/tick.png" />     Creative and modern. Have knowledge of the creative requirements and use of modern tools for teaching.</li>
									<li><img src="images/tick.png" />     Can work with diverse people.</li>
									<li><img src="images/tick.png" />     Enthusiastic about teaching and can pass knowledge with ease.</li>
									<li><img src="images/tick.png" />     Artistically fun-loving.</li>
									<li><img src="images/tick.png" />     Patience is a virtue, character a noble enterprise.</li>
									<li><img src="images/tick.png" />     The rector should possess these with multitasking ability.</li>
									<li><img src="images/tick.png" />     Good Communication Skills</li>
									<li><img src="images/tick.png" />     Team Player</li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What we Expect from you as a Rector?</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Plan the curriculum of engagement with management, tutors and project manager</li>
									<li><img src="images/tick.png" />     Plan the weekly to monthly runs, activities, campaign and others. He/she Works with other departments to achieve this.</li>
									<li><img src="images/tick.png" />     Control the flow of activities in the learning sessions.</li>
									<li><img src="images/tick.png" />     Organize recreational activities in line with program.</li>
									<li><img src="images/tick.png" />     Organize practical work sessions and interactions between students and external industry's guru.</li>
									<li><img src="images/tick.png" />     Monitor and regulate project work of students.</li>
									<li><img src="images/tick.png" />     Plan with other departments, project manager and management on the project work output for the year.</li>
									
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What we Expect from you as a Tutor?</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Guide, instruct and mentor students on the scope of each course.</li>
									<li><img src="images/tick.png" />     Design activities in conjunction with project manager.</li>
									<li><img src="images/tick.png" />     Implement activities and monitor compliance.</li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What you've got?</div>
							<div class="acc_content clearfix">You'll be familiar with creative writing as well as a chance to work with great team members .</div>

						</div>

						<a href="#" data-scrollto="#job-apply" class="button button-3d nomargin">Apply Now</a>

						<div class="divider divider-short"><img src="images/star.png" /></div>


						<div class="fancy-title title-bottom-border">
							<h3>Reporter</h3>
						</div>

						<p>Our reporters work in hand with reviewers on theatre events, production and opportunities. They are our information base and comb for valuable information for our customers in the movie, stage and literary industry.

						This job is research based and behind the scene but not secluded</p>

						<div class="accordion accordion-bg clearfix">

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           Requirements</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     A graduate (inclusive of OND, HND) or undergraduate of mass communication.</li>
									<li><img src="images/tick.png" />     Research sourcing ability.</li>
									<li><img src="images/tick.png" />     Keen interest for information and opportunities dissemination.</li>
									<li><img src="images/tick.png" />     Attention to details.</li>
									<li><img src="images/tick.png" />     An internet savvy personae, This is important since most information resides on the internet.</li>
									<li><img src="images/tick.png" />     Candidate must have the patience to surf for vital information from different organizations and website especially in the outlook for opportunities that will serve our community.</li>
									<li><img src="images/tick.png" />     Excellent report writing skills and creativity.</li>
									<li><img src="images/tick.png" />     Good Communication Skills.</li>
									<li><img src="images/tick.png" />     Ability to use initiative.</li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What we Expect from you?</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Carryout research on opportunity available in our interest field.</li>
									<li><img src="images/tick.png" />     Translate these to information for the customers.</li>
									<li><img src="images/tick.png" />     Attend theatrical productions, movie premiere and art exhibitions. </li>
									<li><img src="images/tick.png" />     Update customers and clients on recent events and trends.</li>
								</ul>
							</div>


							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What you've got?</div>
							<div class="acc_content clearfix">You'll be familiar with creative writing as well as a chance to work with great team members.</div>

						</div>

						<a href="#" data-scrollto="#job-apply" class="button button-3d nomargin">Apply Now</a>

						<div class="divider divider-short"><img src="images/star.png" /></div>
							

						<div class="fancy-title title-bottom-border">
							<h3>Reviewers</h3>
						</div>

						<p>This the fun part of SL Kreativez and it's essentially one of our most priced field. Opportunity is open for literature review and we're focusing on Nigeria at the moment<br>
						Nollywood movie reviews and critique. For the greater path, reviews will be for cinematic movies and at other times, our sledgehammer will fall on our alaba-idumota made movies. <br>

						That's fun, we know but it's also a professional endeavor that requires constructive criticism that aligns to the tenets of a well-made play. 

						We're not here for fault finding but to promote art and energize creativity</p>

						<div class="accordion accordion-bg clearfix">

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           Requirements</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Candidate for this opportunity should be a graduate or an undergraduate of any art field broadly. More specifically, someone from creative arts, theatre arts, film arts or media will be better suited. </li>
									<li><img src="images/tick.png" />     There will be a brush up training for the qualified candidate and a continuous support system till they're able to master the task.</li>
									<li><img src="images/tick.png" />     Passion is key. Candidate must be a big fan of motion pictures.</li>
									<li><img src="images/tick.png" />     Creative and aesthetic judgment skills.</li>
									<li><img src="images/tick.png" />     A creative-analytical orientation and a critical mindset is equally important for this job.</li>
									<li><img src="images/tick.png" />     An unabashed spirit for the criticism that will come with the terrain. A critic must also welcome criticism cheerfully.</li>
									<li><img src="images/tick.png" />     Good communications skills in writing and voice.</li>
									<li><img src="images/tick.png" />     Dexterity of language and creative arts terminology. Candidate in addition to an excellent prowess of the English language, must be able to speak any one or two of these languages fluently: Yoruba, Igbo, Hausa, Efik and Benin. </li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What we Expect from you?</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Attend theatrical productions, movie premiere and cinema.</li>
									<li><img src="images/tick.png" />     Review and critique theatrical productions and movies.</li>
									<li><img src="images/tick.png" />     Read, review and critique literatures.</li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What you've got?</div>
							<div class="acc_content clearfix">You'll be familiar with creative writing as well as a chance to work with great team members.</div>

						</div>

						<a href="#" data-scrollto="#job-apply" data-highlight="yellow" class="button button-3d nomargin">Apply Now</a>

					</div>

					<div class="col_two_fifth nobottommargin col_last">

						<div class="fancy-title title-bottom-border">
							<h3>Freelancers and Interns</h3>
						</div>

						<p>We're looking for art enthusiasts and professionals with different background who want to make a difference in the creative industry. 

						Do you have the skill and flair? Or you have the flair but little skills? We can work together. 

						Whether a recent graduates, university undergraduates or senior secondary school students, who can write or work as freelancers or interns, this is for you. 

						We offer freelancers remuneration for their creative endeavor but also celebrate them through endorsement and profiling. Interns will learn on the job while developing their skills through practical activities and projects</p>

						<div class="accordion accordion-bg clearfix">

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           Requirements</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Recent Graduate, Undergraduate or Secondary school Student</li>
									<li><img src="images/tick.png" />     Enthusiastic about Art and writing.</li>
									<li><img src="images/tick.png" />     Good understanding of what writing entails.</li>
									<li><img src="images/tick.png" />     Good Communication Skills</li>
									<li><img src="images/tick.png" />     Team Player</li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What we Expect from you?</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     </li>
									<li><img src="images/tick.png" />     </li>
									<li><img src="images/tick.png" />     </li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What you've got?</div>
							<div class="acc_content clearfix">You'll be familiar with creative writing as well as a chance to work with great team members.</div>

						</div>

						<a href="#" data-scrollto="#job-apply" class="button button-3d nomargin">Apply Now</a>

						<div class="divider divider-short"><img src="images/star.png" /></div>

						<div class="fancy-title title-bottom-border">
							<h3>Write and Earn</h3>
						</div>

						<p>We have the Write and Earn option for those who just want to share stories with us. Only criteria for this is that the stories must be creative and impacting. Terms and conditions apply

						The choice is yours, take the BOLD step NOW.
						</p>

						<a href="#" data-scrollto="#job-apply" class="button button-3d nomargin">Apply Now</a>

						<div class="divider divider-short"><img src="images/star.png" /></div>



						<div class="fancy-title title-bottom-border">
							<h3>Photography Partner</h3>
						</div>

						<p>Are you a driven creative photographer and understands our dream, vision and rigors of a startup. We appreciate that we can work with you in our little beginning and grow to greatness together. Please reach us at info@seekerslocus.com

						</p>


						<div class="divider divider-short"><img src="images/star.png" /></div>


						<div class="fancy-title title-bottom-border">
							<h3>Project Manager</h3>
						</div>

						<div class="accordion accordion-bg clearfix">

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           Requirements</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     An art enthusiast.</li>
									<li><img src="images/tick.png" />     Project management pedigree.</li>
									<li><img src="images/tick.png" />     A graduate degree with professional training in a relevant field.</li>
									<li><img src="images/tick.png" />     Understanding of what we're about.</li>
									<li><img src="images/tick.png" />     Good Communication Skills</li>
									<li><img src="images/tick.png" />     Team Player</li>
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What we Expect from you?</div>
							<div class="acc_content clearfix">
								<ul class="iconlist iconlist-color nobottommargin">
									<li><img src="images/tick.png" />     Will manage and implement all our projects from start to finish.</li>
									<li><img src="images/tick.png" />     Work closely with management and the team to enhance customers experience.</li>
									
								</ul>
							</div>

							<div class="acctitle"><img src="images/open.png" />     <img src="images/closed.png" />           What you've got?</div>
							<div class="acc_content clearfix">You'll be familiar with creative writing as well as a chance to work with great team members .</div>

						</div>

						<a href="#" data-scrollto="#job-apply" class="button button-3d nomargin">Apply Now</a>

						<div class="divider divider-short"><img src="images/star.png" /></div>



						<div class="fancy-title title-bottom-border">
							<h3>Project Volunteers</h3>
						</div>

						<p>This is big and so are your time and resources. We're having three grand events for the year 2018 and we need your help and support to achieve the goal of these projects. You can choose to be part of one or all of the three. We will award certificate of volunteering to each volunteer and something nice too for your efforts.Please if you're interested, click the button below


						</p>

						<a href="#" data-scrollto="#volunteer-apply" class="button button-3d nomargin">Volunteer</a>

						<div class="divider divider-short"><img src="images/star.png" /></div>

						<div id="job-apply" class="heading-block highlight-me">
							<h2>Apply Now</h2>
							<span>And we'll get back to you within 48 hours.</span>
						</div>

						<div class="contact-widget">

							<div class="contact-form-result"></div>

							<form action="http://themes.semicolonweb.com/html/canvas/dark/include/jobs.php" id="template-jobform" name="template-jobform" method="post" role="form">

								<div class="form-process"></div>

								<div class="col_half">
									<label for="template-jobform-fname">First Name <small>*</small></label>
									<input type="text" id="template-jobform-fname" name="template-jobform-fname" value="" class="sm-form-control required" />
								</div>

								<div class="col_half col_last">
									<label for="template-jobform-lname">Last Name <small>*</small></label>
									<input type="text" id="template-jobform-lname" name="template-jobform-lname" value="" class="sm-form-control required" />
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-jobform-email">Email <small>*</small></label>
									<input type="email" id="template-jobform-email" name="template-jobform-email" value="" class="required email sm-form-control" />
								</div>

								<div class="col_full">
									<label for="template-jobform-city">City <small>*</small></label>
									<input type="text" name="template-jobform-city" id="template-jobform-city" value="" size="22" tabindex="5" class="sm-form-control required" />
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-jobform-service">Position <small>*</small></label>
									<select name="template-jobform-position" id="template-jobform-position"  tabindex="9" class="sm-form-control required">
										<option value="">-- Select Position --</option>
										<option value="Freelancers and Interns">Freelancers and Interns</option>
										<option value="Write and Earn">Write and Earn</option>
										<option value="Creative Writer">Creative Writer</option>
										<option value="Social Media / Community Manager">Social Media / Community Manager</option>
										<option value="Web Content Editor">Web Content Editor</option>
										<option value="Reporter">Reporter</option>
										<option value="Reviewers">Reviewers</option>
										<option value="Photography Partner">Photography Partner</option>
										
									</select>
								</div>

								<div class="col_half">
									<label for="template-jobform-salary">Expected Salary</label>
									<input type="text" name="template-jobform-salary" id="template-jobform-salary" value="" size="22" tabindex="6" class="sm-form-control" />
								</div>

								<div class="col_half col_last">
									<label for="template-jobform-time">Start Date</label>
									<input type="text" name="template-jobform-start" id="template-jobform-start" value="" size="22" tabindex="7" class="sm-form-control" />
								</div>

								<div class="clear"></div>


								<div class="col_full">
									<label for="template-jobform-experience">Experience (optional)</label>
									<textarea name="template-jobform-experience" id="template-jobform-experience" rows="3" tabindex="10" class="sm-form-control"></textarea>
								</div>

								<div class="col_full">
									<label for="template-jobform-application">Application <small>*</small></label>
									<textarea name="template-jobform-application" id="template-jobform-application" rows="6" tabindex="11" class="sm-form-control required"></textarea>
								</div>

								<div class="col_full hidden">
									<input type="text" id="template-jobform-botcheck" name="template-jobform-botcheck" value="" class="sm-form-control" />
								</div>

								<div class="col_full">
									<button class="button button-3d button-large btn-block nomargin" name="template-jobform-apply" type="submit" value="apply">Send Application</button>
								</div>

							</form>

						</div>

						<br><div class="divider divider-short"><img src="images/star.png" /></div>

						<div id="volunteer-apply" class="heading-block highlight-me">
							<h2>Global Volunteer</h2>
							<span>For Interested Parties.</span>
						</div>

						<div class="contact-widget">

							<div class="contact-form-result"></div>

							<form action="http://themes.semicolonweb.com/html/canvas/dark/include/jobs.php" id="template-jobform" name="template-jobform" method="post" role="form">

								<div class="form-process"></div>

								<div class="col_half">
									<label for="template-jobform-fname">First Name <small>*</small></label>
									<input type="text" id="template-jobform-fname" name="template-jobform-fname" value="" class="sm-form-control required" />
								</div>

								<div class="col_half col_last">
									<label for="template-jobform-lname">Last Name <small>*</small></label>
									<input type="text" id="template-jobform-lname" name="template-jobform-lname" value="" class="sm-form-control required" />
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-jobform-email">Email <small>*</small></label>
									<input type="email" id="template-jobform-email" name="template-jobform-email" value="" class="required email sm-form-control" />
								</div>

								<div class="col_half">
									<label for="template-jobform-age">Contact Number <small>*</small></label>
									<input type="text" name="template-jobform-age" id="template-jobform-age" value="" size="22" tabindex="4" class="sm-form-control required" />
								</div>

								<div class="col_half col_last">
									<label for="template-jobform-city">City <small>*</small></label>
									<input type="text" name="template-jobform-city" id="template-jobform-city" value="" size="22" tabindex="5" class="sm-form-control required" />
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-jobform-service">Where will you like to work <small>*</small></label>
									<select name="template-jobform-position" id="template-jobform-position"  tabindex="9" class="sm-form-control required">
										<option value="">-- Select Department --</option>
										<option value="Social Media">Social Media</option>
										<option value="Event Planning and Management">Event Planning and Management</option>
										<option value="Ushering">Ushering</option>
										<option value="Protocol">Protocol</option>
										<option value="Media and Sound">Media and Sound</option>
										<option value="Video and Internet Live Streaming">Video and Internet Live Streaming</option>
										<option value="Photography">Photography</option>
										<option value="Welfare">Welfare</option>
										<option value="Vendor Management">Vendor Management</option>
										<option value="Stage Hands">Stage Hands</option>
										<option value="Red Carpet">Red Carpet</option>
										<option value="Publicity">Publicity</option>
									</select>
								</div>


								<div class="col_full">
									<label for="template-jobform-application">How did you hear about this opportunity? <small>*</small></label>
									<textarea name="template-jobform-application" id="template-jobform-application" rows="6" tabindex="11" class="sm-form-control required"></textarea>
								</div>

								<div class="col_full hidden">
									<input type="text" id="template-jobform-botcheck" name="template-jobform-botcheck" value="" class="sm-form-control" />
								</div>

								<div class="col_full">
									<button class="button button-3d button-large btn-block nomargin" name="template-jobform-apply" type="submit" value="apply">Send Application</button>
								</div>

							</form>

						</div>

					</div>

				</div>

			</div>

		</section><!-- #content end -->
	

		<!-- Footer
		============================================= -->
<?php include('footer.php'); ?>