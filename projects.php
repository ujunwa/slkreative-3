<?php include('header.php'); ?>

<!-- Document Title
	============================================= -->
	<title>Upcoming Projects | SL Kreativez</title>


<?php include('nav.php'); ?>

		<!-- Page Title
		============================================= -->
		<section id="page-title" style="background-image: url('images/time.jpg'); padding: 80px 0; ">

			<div class="container clearfix">
				<h1 style="color: white;">Upcoming Projects</h1>
				<span style="color: white;">Support our Mega Projects for 2018</span>
				<ol class="breadcrumb"">
					<li><a href="index.php" style="color: white;">Home</a></li>
					<li><a href="#" style="color: white;">Pages</a></li>
					<li class="active" style="color: white;">Upcoming Projects</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<!-- Portfolio Filter
					============================================= -->
					<ul id="portfolio-filter" class="portfolio-filter clearfix" data-container="#portfolio">

						<li class="activeFilter"><a href="#" data-filter="*">Show All</a></li>
						<li><a href="#" data-filter=".pf-conference">Conferences</a></li>
						<li><a href="#" data-filter=".pf-workshop">Workshops</a></li>
						<!--<li><a href="#" data-filter=".pf-uielements">UI Elements</a></li>
						<li><a href="#" data-filter=".pf-media">Media</a></li>
						<li><a href="#" data-filter=".pf-graphics">Graphics</a></li>-->

					</ul><!-- #portfolio-filter end -->

					<div id="portfolio-shuffle" class="portfolio-shuffle" data-container="#portfolio">
						<img src="images/shuffle.png">
					</div>

					<div class="clear"></div>

					<!-- Portfolio Items
					============================================= -->
					<div id="portfolio" class="portfolio grid-container portfolio-1 clearfix">

						<article class="portfolio-item pf-conference clearfix">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="images/creative-writing-2.jpeg" alt="2018 Writing Campaign">
								</a>
								<div class="portfolio-overlay">
									<a href="images/creative-writing.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
									<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="#">2018 Writing Campaign</a></h3>
								<span><a href="#">Support our Mega Projects for 2018</a></span>
								<p>From 2018, we will be running a writing campaign for students of low/middle class backgrounds. We will be training 10 selected students in the art of creative writing from January to December. </p>
								<ul class="iconlist">
									<li><strong>Date:</strong> January - December, 2018</li>
									
								</ul>
								<a href="images/5060.jpg" data-lightbox="image" class="button button-3d noleftmargin">Make a Donation</a>
							</div>
						</article>

						<article class="portfolio-item pf-conference clearfix">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="images/meeting.jpeg" alt="Young Creators Workshop">
								</a>
								<div class="portfolio-overlay">
									<a href="images/meeting-2.jpeg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
									<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="#">2018 Young Creators Conference/Seminar</a></h3>
								<span><a href="#">Theme: Resonating Our Identity (ROI)</a></span>
								<p>The YCC project will hold in 2018 with 150 selected delegates of creative art enthusiasts in the following field: Acting, Writing/scripting, Dance, Poetry</p>
								<ul class="iconlist">
									<li><strong>Aim:</strong>To nurture the spirit and hunger for creativity in the delegates. To network participants which includes the delegates, stakeholders and industry's players. To enjoy art and promote it.</li>
									
								</ul>
								<a href="images/5060.jpg" data-lightbox="image" class="button button-3d noleftmargin">Make a Donation</a>
							</div>
						</article>

						<article class="portfolio-item pf-workshop clearfix">
							<div class="portfolio-image">
								<a href="#">
									<img src="images/workshop.jpeg" alt="Young Creators Workshop">
								</a>
								<div class="portfolio-overlay">
									<a href="images/workshop-2.jpeg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
									<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">2018 Young Creators Workshop</a></h3>
								<span><a href="#">Theme: Resonating Our Identity (ROI)</a></span>
								<p>The YCW project will hold in the summer of 2018 with training for youngsters between age 12 to 20. Training will cover the following fields: Craft making, Leather Bag Making, Crotchet Fabric Making, Waste recycling, Footwear Making.</p>
								<ul class="iconlist">
									<li><strong>Details:</strong> Full information coming soon</li>
								</ul>
								<a href="images/5060.jpg" data-lightbox="image" class="button button-3d noleftmargin">Make a Donation</a>
							</div>
						</article>

						<!-- Modal -->



					</div><!-- #portfolio end -->
					<div class="modal1 mfp-hide subscribe-widget" id="coming">
						<div class="block dark divcenter" style="background: url('images/5060.jpg') no-repeat; background-size: cover; max-width: 700px;" data-height-lg="400">
					</div>
						

				</div>

			</div>

		</section><!-- #content end -->


<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix" >

					
					<div class="col-md-6 widget subscribe-widget clearfix" style="margin-bottom: 50px;">
						<h4><img src="images/copy2.jpg"> Copyright</h4> 
						<p align="justify">Hi, as you know most of the information and contents on this website are the intellectual properties of creative individuals. It wouldn't be illegal to use their work without  proper request and attribution. 
						To this end, all contents on seekerslocus.com and other information contained are the exclusive property of SL KREATIVEZ and her partners. They can be shared on social media but any other use must come with express permission from us. When used for educational purposes, users must reference us appropriately. Any commercial use must be with a permission of copyright use. We take the right permission when we use contents from third parties, and they’re properly attributed. <br>You can reach us via info@seekerslocus.com, Thank You.
						</p>
						<div class="clearfix"></div>
						<br>
						<h4><strong>Subscribe</strong> to Our Newsletter to get Important News and latest opporrtunities:</h4>
						<div class="widget-subscribe-form-result"></div>
						<form id="widget-subscribe-form" action="http://themes.semicolonweb.com/html/canvas/include/subscribe.php" role="form" method="post" class="nobottommargin">
							<div class="input-group divcenter">
								
								<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email" style="height:40px;">
								<span class="input-group-btn">
									<button class="button button-3d noleftmargin" style="margin-top:0px;" type="submit">Subscribe</button>
								</span>
							</div>
						</form>
					</div>


					<div class="col-md-6 widget subscribe-widget clearfix" style="margin-top:-5px">
						<h4><img src="images/insta2.jpg"> INSTAGRAM FEEDS </h4>
						<p>
							<iframe src="//users.instush.com/bee-gallery-v2/?cols=5&rows=2&size=small&border=10&round=false&space=4&sl=true&bg=transparent&brd=true&na=false&pin=true&hc=e72476&ltc=3f3f3f&lpc=ffffff&fc=ffffff&user_id=1474819067&username=_ujunwa&sid=-1&susername=-1&tag=-1&stype=mine&t=999999O3mfm-kwleBjgBevNAu0_y4fA-y4hJ8dX5Bx-BUc1Tun1lYnHYIOerZYpdh0sJr5hP59mHe15gs" allowtransparency="true" frameborder="0" scrolling="no"  style="display:block;width:548px;height:224px;border:none;overflow:visible;" ></iframe>
						</p>

						
						<h4>Graphics Designer:<span style="font-size: 13px;"> Samson Ochuko, professional graphics artist and CEO of Phebony Graphics for the Seamless logo and site banner.</span></h4>
						<h4>Images:<span style="font-size: 13px;">pixabay.com, pexels.com for the free to use stock images.</span></h4>
					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div style="margin-bottom:0px; padding-top: 40px; background-color: rgba(0,0,0,0.2);">

				<div class="container clearfix">

					<div class="col_full widget subscribe-widget clearfix" align="center">
						<h4>Copyrights SL Kreativez &copy; 2017 | <span style="font-size: 13px;"> Designed by Rosie Andre (iro3 designs) </h4>
					</div>
	

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="assets/js/jquery.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="assets/js/functions.js"></script>

	<script type="text/javascript" src="assets/js/new.js"></script>

	
<script>jQuery(document).ready(function(e){e("#primary-menu > ul li").find('.new-badge').children("div").append('<span class="label label-danger" style="display:inline-block;margin-left:8px;position:relative;top:-1px;text-transform:none;">New</span>')});</script>