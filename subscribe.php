<?php include('header.php'); ?>

<!-- Document Title
	============================================= -->
	<title>Subscribe | SL Kreativez</title>


<?php include('nav.php'); ?>

		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Modal on Load</h1>
				<span>Subscription Form on Image Background</span>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Shortcodes</a></li>
					<li class="active">Modal on Load</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="center">
						<a href="#myModal1" data-lightbox="inline" class="button button-large button-rounded">Trigger Modal</a>
						<div class="line"></div>
					</div>

					<div class="modal-on-load" data-target="#myModal1"></div>

					<!-- Modal -->
					<div class="modal1 mfp-hide subscribe-widget" id="myModal1">
						<div class="block dark divcenter" style="background: url('images/coming.jpeg') no-repeat; background-size: cover; max-width: 700px;" data-height-lg="400">
							<div style="padding: 50px;">
								<div class="heading-block nobottomborder bottommargin-sm" style="max-width:500px;">
									<h3>Newsletter Subscribe</h3>
									<span>Get Latest Fashion Updates &amp; Offers</span>
								</div>
								<div class="widget-subscribe-form-result"></div>
								<form class="widget-subscribe-form2" action="http://themes.semicolonweb.com/html/canvas/include/subscribe.php" role="form" method="post" style="max-width: 350px;">
									<input type="email" id="widget-subscribe-form2-email" name="widget-subscribe-form-email" class="form-control input-lg not-dark required email" placeholder="Enter your Email">
									<button class="button button-rounded button-border button-light noleftmargin" type="submit" style="margin-top:15px;">Subscribe</button>
								</form>
								<p class="nobottommargin"><small><em>*We hate Spam &amp; Junk Emails.</em></small></p>
							</div>
						</div>
					</div>

				</div>

			</div>

		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">

					<div class="col_two_third">

						<div class="col_one_third">

							<div class="widget clearfix">

								<img src="canvashtml-cdn.semicolonweb.com/images/footer-widget-logo.png" alt="" class="footer-logo">

								<p>We believe in <strong>Simple</strong>, <strong>Creative</strong> &amp; <strong>Flexible</strong> Design Standards.</p>

								<div style="background: url('canvashtml-cdn.semicolonweb.com/images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>Headquarters:</strong><br>
										795 Folsom Ave, Suite 600<br>
										San Francisco, CA 94107<br>
									</address>
									<abbr title="Phone Number"><strong>Phone:</strong></abbr> (91) 8547 632521<br>
									<abbr title="Fax"><strong>Fax:</strong></abbr> (91) 11 4752 1433<br>
									<abbr title="Email Address"><strong>Email:</strong></abbr> info@canvas.com
								</div>

							</div>

						</div>

						<div class="col_one_third">

							<div class="widget widget_links clearfix">

								<h4>Blogroll</h4>

								<ul>
									<li><a href="http://codex.wordpress.org/">Documentation</a></li>
									<li><a href="http://wordpress.org/support/forum/requests-and-feedback">Feedback</a></li>
									<li><a href="http://wordpress.org/extend/plugins/">Plugins</a></li>
									<li><a href="http://wordpress.org/support/">Support Forums</a></li>
									<li><a href="http://wordpress.org/extend/themes/">Themes</a></li>
									<li><a href="http://wordpress.org/news/">WordPress Blog</a></li>
									<li><a href="http://planet.wordpress.org/">WordPress Planet</a></li>
								</ul>

							</div>

						</div>

						<div class="col_one_third col_last">

							<div class="widget clearfix">
								<h4>Recent Posts</h4>

								<div id="post-list-footer">
									<div class="spost clearfix">
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
											</div>
											<ul class="entry-meta">
												<li>10th July 2014</li>
											</ul>
										</div>
									</div>

									<div class="spost clearfix">
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
											</div>
											<ul class="entry-meta">
												<li>10th July 2014</li>
											</ul>
										</div>
									</div>

									<div class="spost clearfix">
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
											</div>
											<ul class="entry-meta">
												<li>10th July 2014</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>

					<div class="col_one_third col_last">

						<div class="widget clearfix" style="margin-bottom: -20px;">

							<div class="row">

								<div class="col-md-6 bottommargin-sm">
									<div class="counter counter-small"><span data-from="50" data-to="15065421" data-refresh-interval="80" data-speed="3000" data-comma="true"></span></div>
									<h5 class="nobottommargin">Total Downloads</h5>
								</div>

								<div class="col-md-6 bottommargin-sm">
									<div class="counter counter-small"><span data-from="100" data-to="18465" data-refresh-interval="50" data-speed="2000" data-comma="true"></span></div>
									<h5 class="nobottommargin">Clients</h5>
								</div>

							</div>

						</div>

						<div class="widget subscribe-widget clearfix">
							<h5><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</h5>
							<div class="widget-subscribe-form-result"></div>
							<form id="widget-subscribe-form" action="http://themes.semicolonweb.com/html/canvas/include/subscribe.php" role="form" method="post" class="nobottommargin">
								<div class="input-group divcenter">
									<span class="input-group-addon"><i class="icon-email2"></i></span>
									<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
									<span class="input-group-btn">
										<button class="btn btn-success" type="submit">Subscribe</button>
									</span>
								</div>
							</form>
						</div>

						<div class="widget clearfix" style="margin-bottom: -20px;">

							<div class="row">

								<div class="col-md-6 clearfix bottommargin-sm">
									<a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
										<i class="icon-facebook"></i>
										<i class="icon-facebook"></i>
									</a>
									<a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
								</div>
								<div class="col-md-6 clearfix">
									<a href="#" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;">
										<i class="icon-rss"></i>
										<i class="icon-rss"></i>
									</a>
									<a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a>
								</div>

							</div>

						</div>

					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2014 All Rights Reserved by Canvas Inc.<br>
						<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
					</div>

					<div class="col_half col_last tright">
						<div class="fright clearfix">
							<a href="#" class="social-icon si-small si-borderless si-facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-gplus">
								<i class="icon-gplus"></i>
								<i class="icon-gplus"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-pinterest">
								<i class="icon-pinterest"></i>
								<i class="icon-pinterest"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-vimeo">
								<i class="icon-vimeo"></i>
								<i class="icon-vimeo"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-github">
								<i class="icon-github"></i>
								<i class="icon-github"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-yahoo">
								<i class="icon-yahoo"></i>
								<i class="icon-yahoo"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-linkedin">
								<i class="icon-linkedin"></i>
								<i class="icon-linkedin"></i>
							</a>
						</div>

						<div class="clear"></div>

						<i class="icon-envelope2"></i> info@canvas.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> +91-11-6541-6369 <span class="middot">&middot;</span> <i class="icon-skype2"></i> CanvasOnSkype
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="canvashtml-cdn.semicolonweb.com/js/jquery.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="canvashtml-cdn.semicolonweb.com/js/functions.js"></script>

<div class="switcher clearfix"><div class="switcher-head"><span>Style Switcher<a href="#" id="switcher-reset-button" class="btn btn-danger fright btn-xs" style="margin-top: 12px;">Reset</a></span><div class="switcher-trigger icon-line-cog"></div></div><div class="switcher-body clearfix"><!-- <div id="switcher-op-layout"><a href="#" data-layout="stretched" class="button button-3d button-black" style="margin: 0 10px 0 0;">Wide</a><a href="#" data-layout="boxed" class="button button-3d button-white button-light nomargin">Boxed</a></div><div class="line"></div> --><div class="switcher-op-pt"><h5>Page Loading Styles:</h5><a href="modal-onload-subscribe4d84.html?page-loader=1" class="button button-3d button-small noleftmargin">1</a><a href="modal-onload-subscribefa22.html?page-loader=2" class="button button-3d button-small">2</a><a href="modal-onload-subscribe516e.html?page-loader=3" class="button button-3d button-small noleftmargin">3</a><a href="modal-onload-subscribed0c8.html?page-loader=4" class="button button-3d button-small">4</a><a href="modal-onload-subscribe9b48.html?page-loader=5" class="button button-3d button-small noleftmargin">5</a><a href="modal-onload-subscribe221d.html?page-loader=6" class="button button-3d button-small">6</a><a href="modal-onload-subscribe0d37.html?page-loader=7" class="button button-3d button-small">7</a><a href="modal-onload-subscribe2614.html?page-loader=8" class="button button-3d button-small">8</a><a href="modal-onload-subscribe8316.html?page-loader=9" class="button button-3d button-small noleftmargin">9</a><a href="modal-onload-subscribea35a.html?page-loader=10" class="button button-3d button-small">10</a><a href="modal-onload-subscribe5ae2.html?page-loader=11" class="button button-3d button-small">11</a><a href="modal-onload-subscribe17b7.html?page-loader=12" class="button button-3d button-small noleftmargin norightmargin">12</a><a href="modal-onload-subscribe2bdc.html?page-loader=13" class="button button-3d button-small noleftmargin">13</a><a href="modal-onload-subscribecff7.html?page-loader=14" class="button button-3d button-small">14</a></div><div class="line"></div><div class="switcher-op-colors"><h5>Choose Color:</h5><ul id="switcher-option-color" class="switcher-op-selectors clearfix"><li style="background-color: #1abc9c;" data-color="1abc9c"></li><li style="background-color: #3498db;" data-color="3498db"></li><li style="background-color: #9b59b6;" data-color="9b59b6"></li><li style="background-color: #34495e;" data-color="34495e"></li><li style="background-color: #e67e22;" data-color="e67e22"></li><li style="background-color: #c0392b;" data-color="c0392b"></li><li style="background-color: #336E7B;" data-color="336E7B"></li><li style="background-color: #3A539B;" data-color="3A539B"></li><li style="background-color: #019875;" data-color="019875"></li></ul></div><div class="line"></div><!-- <div class="switcher-op-patterns"><p><strong>Choose Pattern:</strong></p><ul id="switcher-option-pattern" class="switcher-op-selectors clearfix"><li style="background-image: url('switcher/patterns/light/pattern1.png');" data-url="switcher/patterns/light/pattern1.png"></li><li style="background-image: url('switcher/patterns/light/pattern3.png');" data-url="switcher/patterns/light/pattern3.png"></li><li style="background-image: url('switcher/patterns/light/pattern4.png');" data-url="switcher/patterns/light/pattern4.png"></li><li style="background-image: url('switcher/patterns/light/pattern6.png');" data-url="switcher/patterns/light/pattern6.png"></li><li style="background-image: url('switcher/patterns/light/pattern7.png');" data-url="switcher/patterns/light/pattern7.png"></li><li style="background-image: url('switcher/patterns/light/pattern8.png');" data-url="switcher/patterns/light/pattern8.png"></li><li style="background-image: url('switcher/patterns/light/pattern9.png');" data-url="switcher/patterns/light/pattern9.png"></li><li style="background-image: url('switcher/patterns/light/pattern10.png');" data-url="switcher/patterns/light/pattern10.png"></li><li style="background-image: url('switcher/patterns/light/pattern13.png');" data-url="switcher/patterns/light/pattern13.png"></li><li style="background-image: url('switcher/patterns/dark/pattern1.png');" data-url="switcher/patterns/dark/pattern1.png"></li><li style="background-image: url('switcher/patterns/dark/pattern2.png');" data-url="switcher/patterns/dark/pattern2.png"></li><li style="background-image: url('switcher/patterns/dark/pattern3.png');" data-url="switcher/patterns/dark/pattern3.png"></li><li style="background-image: url('switcher/patterns/dark/pattern4.png');" data-url="switcher/patterns/dark/pattern4.png"></li><li style="background-image: url('switcher/patterns/dark/pattern5.png');" data-url="switcher/patterns/dark/pattern5.png"></li><li style="background-image: url('switcher/patterns/dark/pattern6.png');" data-url="switcher/patterns/dark/pattern6.png"></li><li style="background-image: url('switcher/patterns/dark/pattern7.png');" data-url="switcher/patterns/dark/pattern7.png"></li><li style="background-image: url('switcher/patterns/dark/pattern8.png');" data-url="switcher/patterns/dark/pattern8.png"></li><li style="background-image: url('switcher/patterns/dark/pattern9.png');" data-url="switcher/patterns/dark/pattern9.png"></li></ul></div><div class="line"></div><div class="switcher-op-bgimages"><p><strong>Background Images:</strong></p><ul id="switcher-option-bgimage" class="switcher-op-selectors clearfix"><li data-url="images/parallax/home/1.jpg"><img src="switcher/bgimages/1.jpg" alt="Background Image 1" title="Background Image 1"/></li><li data-url="images/parallax/home/2.jpg"><img src="switcher/bgimages/2.jpg" alt="Background Image 2" title="Background Image 2"/></li><li data-url="images/parallax/home/4.jpg"><img src="switcher/bgimages/4.jpg" alt="Background Image 3" title="Background Image 3"/></li><li data-url="images/parallax/home/5.jpg"><img src="switcher/bgimages/5.jpg" alt="Background Image 4" title="Background Image 4"/></li><li data-url="images/parallax/home/6.jpg"><img src="switcher/bgimages/6.jpg" alt="Background Image 5" title="Background Image 5"/></li><li data-url="images/parallax/home/7.jpg"><img src="switcher/bgimages/7.jpg" alt="Background Image 6" title="Background Image 6"/></li><li data-url="images/parallax/home/9.jpg"><img src="switcher/bgimages/9.jpg" alt="Background Image 7" title="Background Image 7"/></li><li data-url="images/parallax/home/10.jpg"><img src="switcher/bgimages/10.jpg" alt="Background Image 8" title="Background Image 8"/></li><li data-url="images/parallax/home/11.jpg"><img src="switcher/bgimages/11.jpg" alt="Background Image 9" title="Background Image 9"/></li></ul></div><div class="line"></div> --><div id="switcher-option-footer" class="switcher-op-footerc clearfix"><a href="dark/modal-onload-subscribe.html" class="button button-3d button-large button-black center noleftmargin notopmargin nobottommargin" style="padding:0 25px;">Dark</a><a href="intro.html" class="button button-3d button-large button-black center nomargin" style="padding:0 25px;">Demos</a></div></div></div><script type="text/javascript" src="switcher/switcher.js"></script><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-23255544-12', 'auto');
  ga('send', 'pageview');
</script><script>jQuery(document).ready(function(e){e("#primary-menu > ul li").find('.new-badge').children("div").append('<span class="label label-danger" style="display:inline-block;margin-left:8px;position:relative;top:-1px;text-transform:none;">New</span>')});</script>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"839484a19a","applicationID":"5289971","transactionName":"ZQEDZxZUD0FZVkxfX1xLNEENGglGVVkXVVFcEgBAS1gOVllZFVleXgsAV0lGFFBLVkpfUldKEVsU","queueTime":0,"applicationTime":1,"atts":"SUYAEV5OHE8=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>

<!-- Mirrored from themes.semicolonweb.com/html/canvas/modal-onload-subscribe.php by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 Sep 2017 05:21:21 GMT -->
</html>